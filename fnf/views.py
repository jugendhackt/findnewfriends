from django.shortcuts import render, redirect
from django.http import HttpResponse
from fnf.models import *
from fnf.forms import *
import django.contrib.auth

def index(request):
    hobbies = Hobby.objects.all()
    return render(request, "fnf/home.html", {'hobbies': hobbies})

def map(request):
    return render(request, "fnf/map.html")

def login(request):
    is_error = False
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = django.contrib.auth.authenticate(request, username = username, password = password)
            if user is not None:
                django.contrib.auth.login(request, user)
                return redirect("admin:index")
            else:
                is_error = True
        else:
            is_error = True
    else:
        form = LoginForm()
    return render(request, "fnf/login.html", {"form": form, "is_error": is_error})

def register(request):
    return render(request, "fnf/register.html")

