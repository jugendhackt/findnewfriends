from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.
class Hobby(models.Model):
    name = models.CharField(max_length=255)
    user_hobby = models.ManyToManyField(User, related_name="user_hobby", through='UserHobby')

    def __str__(self):
        return self.name

class UserHobby(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    hobby = models.ForeignKey(Hobby, on_delete=models.CASCADE)
    skills = models.TextField(blank = True, null = True)
    since = models.DateTimeField(blank = True, null = True)   

    def __str__(self):
        return self.user.username + ' - ' + self.hobby.name

