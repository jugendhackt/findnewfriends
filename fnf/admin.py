from django.contrib import admin
from fnf.models import *

class HobbyAdmin(admin.ModelAdmin):
    fields = ["name"]

class UserHobbyAdmin(admin.ModelAdmin):
    fields = ["user","hobby","skills","since"]

admin.site.register(Hobby, HobbyAdmin)
admin.site.register(UserHobby, UserHobbyAdmin)