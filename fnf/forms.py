from django import forms
from fnf.models import *

class LoginForm(forms.Form):
    username = forms.CharField(label = "Benutzername")
    password = forms.CharField(label = "Passwort", widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    email = forms.EmailField(label = "Email-Adresse")
    username = forms.CharField(label = "Benutzername")
    password = forms.CharField(label = "Passwort", widget=forms.PasswordInput)
    repeat_password= forms.CharField(label = "wiederhole Passwort", widget=forms.PasswordInput)
