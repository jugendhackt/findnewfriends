from django.urls import path

from . import views

app_name="fnf"
urlpatterns = [
    path('', views.index, name='index'),
    path('map',views.map,name='map'),
    path('detail/mate-trinken',views.index),
    path('login',views.login,name='login'),
    path('register',views.register,name='register'),
]
